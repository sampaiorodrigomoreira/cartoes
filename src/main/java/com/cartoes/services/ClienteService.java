package com.cartoes.services;

import com.cartoes.DTOs.InsereClienteDTO;
import com.cartoes.models.Cliente;
import com.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public ClienteService(){}

    public Cliente cadastrarCliente(InsereClienteDTO clienteDTO){
        Cliente cliente = new Cliente();
        cliente.setName(clienteDTO.getNome());
        return clienteRepository.save(cliente);
    }

    public Cliente buscarClientePeloId(int id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent()){
            Cliente cliente = clienteOptional.get();
            return cliente;
        }else {
            throw new RuntimeException("Cliente não encontrado!");
        }
    }



}
