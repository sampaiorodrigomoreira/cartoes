package com.cartoes.services;

import com.cartoes.DTOs.AtualizaCartaoAtivoDTO;
import com.cartoes.DTOs.CadastraCartaoRequestDTO;
import com.cartoes.DTOs.CartaoResponseDTO;
import com.cartoes.DTOs.ExibirCartaoDTO;
import com.cartoes.models.Cartao;
import com.cartoes.models.Cliente;
import com.cartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    ClienteService clienteService;

    @Autowired
    CartaoRepository cartaoRepository;

    public CartaoResponseDTO cadastraCartao(CadastraCartaoRequestDTO cadastraCartaoRequestDTO){
        Cliente cliente = new Cliente();
        Cartao inserircartao = new Cartao();

        cliente = clienteService.buscarClientePeloId(cadastraCartaoRequestDTO.getClienteId());

        inserircartao.setAtivo(false);
        inserircartao.setClienteProprietario(cliente);
        inserircartao.setNumero(cadastraCartaoRequestDTO.getNumero());

        Cartao novoCartao = cartaoRepository.save(inserircartao);

        CartaoResponseDTO response = new CartaoResponseDTO(
                novoCartao.getId(),
                novoCartao.getNumero(),
                novoCartao.getClienteProprietario(),
                novoCartao.isAtivo()
        );

        return response;
    }

    public ExibirCartaoDTO pesquisarPorNumeroCartao(String numeroCartao){

        try {
            Cartao cartaoPesquisado = cartaoRepository.findByNumero(numeroCartao);

            ExibirCartaoDTO exibirCartaoDTO = new ExibirCartaoDTO();
            exibirCartaoDTO.setId(cartaoPesquisado.getId());
            exibirCartaoDTO.setClienteId(cartaoPesquisado.getId());
            exibirCartaoDTO.setNumero(cartaoPesquisado.getNumero());

            return exibirCartaoDTO;
        } catch (RuntimeException e) {
            throw new RuntimeException("Cartão não encontrado");
        }

    }

    public Cartao pesquisaPorId(int id) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        if(cartaoOptional.isPresent()){
            Cartao cartao = cartaoOptional.get();
            return cartao;
        } else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public Iterable<Cartao> pesquisaCartoes() {
        return cartaoRepository.findAll();
    }

    public CartaoResponseDTO atualizaPorNumeroCartao(String numero, AtualizaCartaoAtivoDTO atualizaCartaoAtivoDTO) {

        CartaoResponseDTO response = new CartaoResponseDTO();
        Cartao cartaoAtualizado = new Cartao();

        if(cartaoRepository.existsByNumero(numero)){
            Cartao cartaoDB = cartaoRepository.findByNumero(numero);

            cartaoAtualizado = cartaoDB;
            cartaoAtualizado.setAtivo(atualizaCartaoAtivoDTO.isAtivo());
            cartaoRepository.save(cartaoAtualizado);

            response.setAtivo(cartaoAtualizado.isAtivo());
            response.setClienteId(cartaoAtualizado.getClienteProprietario());
            response.setId(cartaoAtualizado.getId());
            response.setNumero(cartaoAtualizado.getNumero());
            return response;
        } else {
            throw new RuntimeException("Cartao não existe");
        }
    }

}
