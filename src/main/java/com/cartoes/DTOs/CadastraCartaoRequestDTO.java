package com.cartoes.DTOs;

public class CadastraCartaoRequestDTO {


    private int clienteId;

    private String numero;

    public CadastraCartaoRequestDTO(){}

    public CadastraCartaoRequestDTO(int clienteId, String numero) {
        this.clienteId = clienteId;
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }



}
