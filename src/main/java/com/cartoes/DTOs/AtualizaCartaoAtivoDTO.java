package com.cartoes.DTOs;

public class AtualizaCartaoAtivoDTO {

    private boolean ativo;

    public AtualizaCartaoAtivoDTO(){}

    public AtualizaCartaoAtivoDTO(boolean ativo) {
        this.ativo = ativo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

}
