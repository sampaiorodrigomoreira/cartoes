package com.cartoes.DTOs;

import com.cartoes.models.Cliente;

public class CartaoResponseDTO {

    private int id;
    private String numero;
    private Cliente clienteId;
    private boolean ativo;

    public CartaoResponseDTO() {}

    public CartaoResponseDTO(int id, String numero, Cliente clienteId, boolean ativo) {
        this.id = id;
        this.numero = numero;
        this.clienteId = clienteId;
        this.ativo = ativo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Cliente getClienteId() {
        return clienteId;
    }

    public void setClienteId(Cliente clienteId) {
        this.clienteId = clienteId;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
