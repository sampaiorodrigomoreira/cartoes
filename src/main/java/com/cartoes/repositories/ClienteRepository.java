package com.cartoes.repositories;

import com.cartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.client.ClientHttpRequestExecution;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {}
