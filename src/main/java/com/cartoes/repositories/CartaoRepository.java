package com.cartoes.repositories;

import com.cartoes.models.Cartao;
import org.springframework.data.repository.CrudRepository;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {

    Cartao findByNumero(String numeroCartao);

    boolean existsByNumero(String numeroCartao);
}
