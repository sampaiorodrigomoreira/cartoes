package com.cartoes.controllers;

import com.cartoes.DTOs.AtualizaCartaoAtivoDTO;
import com.cartoes.DTOs.CadastraCartaoRequestDTO;
import com.cartoes.DTOs.CartaoResponseDTO;
import com.cartoes.DTOs.ExibirCartaoDTO;
import com.cartoes.models.Cartao;
import com.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartoes")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoResponseDTO cadastrarCartao(@RequestBody @Valid CadastraCartaoRequestDTO cadastraCartaoRequestDTO){
        try{
            return cartaoService.cadastraCartao(cadastraCartaoRequestDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

    @GetMapping("/{numero}")
    public ExibirCartaoDTO pesquisarPorId(@PathVariable(name="numero") String numero){
        try{
            return cartaoService.pesquisarPorNumeroCartao(numero);
        } catch(RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping
    public Iterable<Cartao> exibeTodosOsCartoes() {
        return cartaoService.pesquisaCartoes();
    }

    @PatchMapping("/{numero}")
    public CartaoResponseDTO ativaCartao(@PathVariable(name = "numero") String numero,
                                         @RequestBody AtualizaCartaoAtivoDTO atualizaCartaoAtivoDTO){
        try {
            return cartaoService.atualizaPorNumeroCartao(numero, atualizaCartaoAtivoDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
