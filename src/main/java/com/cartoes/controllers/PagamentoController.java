package com.cartoes.controllers;

import com.cartoes.DTOs.PagamentoResponseDTO;
import com.cartoes.DTOs.RealizaPagamentoRequestDTO;
import com.cartoes.repositories.PagamentoRepository;
import com.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/pagamentos")
public class PagamentoController {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    PagamentoService pagamentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoResponseDTO registrarPagamento(@RequestBody @Valid RealizaPagamentoRequestDTO pagamentoDTO) {
        try{
            return pagamentoService.realizaPagamento(pagamentoDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{numero}")
    public List<PagamentoResponseDTO> pagamentosPorCartao(@PathVariable(name = "numero") String numero){
        try{
            return pagamentoService.buscaPagamentosPorCartao(numero);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
